create database contacts;

use contacts;

create table contacts (
    id int not null auto_increment primary key,
    name varchar(200) not null,
    number varchar(15) not null
);